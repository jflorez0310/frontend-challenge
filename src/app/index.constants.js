/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('tienda')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('CONFIG', {
      RANGO:">=and=<",
      MAYOR_IGUAL_QUE:">=",
      MENOR_IGUAL_QUE:"<="
    });

})();
