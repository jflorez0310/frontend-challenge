(function() {
  'use strict';

  angular
    .module('tienda')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(dataMainService) {
      var vm = this;
      vm.dataMainService = dataMainService;
      vm.setCategoria = function(categoria){
        if(categoria === null){
          vm.dataMainService.setCategoria({id: null, name: "All Categories"});
        }else{
          vm.dataMainService.setCategoria(categoria);
        }
      }
    }
  }

})();
