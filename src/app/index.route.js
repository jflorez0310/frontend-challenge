(function() {
  'use strict';

  angular
    .module('tienda')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/home/home.html',
        controller: 'HomeController',
        controllerAs: 'vm'
      })
      .state('car', {
        url: '/car',
        templateUrl: 'app/main/car/car.html',
        controller: 'CarController',
        controllerAs: 'vm'
      });
    $urlRouterProvider.otherwise('/');
  }

})();
