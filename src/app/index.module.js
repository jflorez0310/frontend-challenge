(function() {
  'use strict';

  angular
    .module('tienda', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'toastr', 'ngTagsInput', 'ngProgress']);

})();
