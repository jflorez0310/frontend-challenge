(function (angular) {
  'use strict';

  angular
    .module('tienda')
    .service('carService', ['purchaseFactory',
      carService
    ]);

  function carService(purchaseFactory) {
    var vm = this;

    // Variables


    // Services
    vm.purchaseFactory = purchaseFactory;

    // Methods
    vm.borrar = borrar;
    vm.cargarProductos = cargarProductos;
    vm.saveChange = saveChange;


    return vm;

    function borrar(index){
      vm.productos.splice(index, 1);
      vm.purchaseFactory.set('purchase', vm.productos);
    }

    function saveChange(){
      vm.purchaseFactory.set('purchase', vm.productos);
    }

    function cargarProductos(){
      vm.productos = vm.purchaseFactory.getObject('purchase',[]);
    }


  }
})(window.angular)
