(function() {
  'use strict';

  angular
    .module('tienda')
    .controller('CarController', CarController);

  /** @ngInject */
  function CarController(dataMainService, purchaseFactory, ngProgressFactory, $scope, carService) {
    var vm = this;

    // Variables
    vm.carService = carService;
    $scope.progressbar = ngProgressFactory.createInstance();



    // Methods
    vm.borrar = borrar;

    // Services
    vm.dataMainService = dataMainService;
    vm.purchaseFactory = purchaseFactory;

    vm.carService.cargarProductos();

    function borrar(index){
      vm.carService.borrar(index);
    }


    $scope.$watch(function(){
      return vm.carService.productos;
    }, function (response) {
      vm.carService.saveChange();

    }, true)

  }
})();
