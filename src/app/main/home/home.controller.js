(function() {
  'use strict';

  angular
    .module('tienda')
    .controller('HomeController', HomeController);

  /** @ngInject */
  function HomeController($timeout, $scope, webDevTec, toastr, dataMainService, purchaseFactory, ngProgressFactory) {
    var vm = this;

    // Variables
    vm.awesomeThings = [];
    vm.classAnimation = '';
    vm.creationDate = 1521328794591;
    $scope.progressbar = ngProgressFactory.createInstance();


    // Methods
    vm.showToastr = showToastr;
    vm.addFiltro = addFiltro;

    // Services
    vm.dataMainService = dataMainService;
    vm.purchaseFactory = purchaseFactory;

    vm.dataMainService.buscar(vm.dataMainService.categoriaSelected);


    activate();

    function activate() {
      getWebDevTec();
      $timeout(function () {
        vm.classAnimation = 'rubberBand';
      }, 4000);
    }

    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }

    function getWebDevTec() {
      vm.awesomeThings = webDevTec.getTec();

      angular.forEach(vm.awesomeThings, function (awesomeThing) {
        awesomeThing.rank = Math.random();
      });
    }


    // Agrega un filtrogulp
    function addFiltro(tipo, id){
      if(id !== null && id !== undefined){
        var filtro = null;
        switch(tipo){
          case 2:
            filtro = buildTagPrice(tipo, id);
            break;
          case 3:
            filtro = buildTagStock(tipo, id);
            break;
          case 4:
            filtro = buildTagSortBy(id);
            break;
          case 1:
            filtro = buildTagAvailability(id);
            break;
        }
        vm.dataMainService.addFiltro(filtro);
      }
    }

    function buildTagPrice(tipo, valor){
      var filtro = {};
      filtro.tipo = tipo;
      filtro.valor2 = valor;
      if(valor.min !== null && valor.max !== null){
        filtro.operacion = '>=and=<';
        filtro.valor1 = 'Min: $'+valor.min + ' - Max: $'+valor.max;
      }
      if(valor.min !== null && valor.max === null){
        filtro.operacion = '>=';
        filtro.valor1 = 'Min: $'+valor.min;
      }
      if(valor.min === null && valor.max !== null){
        filtro.operacion = '<=';
        filtro.valor1 = 'Max: $'+valor.max;
      }
      return filtro;

    }

    function buildTagStock(tipo, valor){
      var filtro = {};
      filtro.tipo = 3;
      filtro.valor1 = 'Stock min.: '+valor;
      filtro.valor2 = valor;
      return filtro;
    }

    // Devuelve un objeto filtro del tipo Availability
    function buildTagAvailability(id){
      var parametros = vm.dataMainService.parametros.availability;
      var filtro = buscarFiltro(id, parametros);
      if(filtro !== null){
        filtro.valor1 = 'Availability: '+filtro.name;
      }
      return filtro;
    }


    // Devuelve un objeto filtro del tipo Sort
    function buildTagSortBy(id){
      var parametros = vm.dataMainService.parametros.order;
      var filtro = buscarFiltro(id, parametros);
      if(filtro !== null){
        filtro.valor1 = 'Sort By: '+filtro.name;
      }
      return filtro;
    }


    // Buscar y devuelve un objeto filtro en una lista que recipe por parametro.
    function buscarFiltro(id, lista){
      var filtro = null;
      for(var i = 0; i < lista.length; i++) {
        if (lista[i].id == id) {
          filtro = lista[i];
        }
      }
      return filtro;
    }





    $scope.$watch(function(){
      return vm.dataMainService.categoriaSelected;
    }, function (response) {
      $scope.progressbar.start();
      vm.dataMainService.buscar(vm.dataMainService.palabra);
      $scope.progressbar.complete();
    }, true)



    $scope.$watch(function(){
      return vm.dataMainService.filtros;
    }, function (response) {
      $scope.progressbar.start();
      vm.dataMainService.buscar(vm.dataMainService.palabra);
      $scope.progressbar.complete();
    }, true)
  }
})();
