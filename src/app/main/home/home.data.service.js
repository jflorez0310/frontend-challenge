(function (angular) {
  'use strict';

  angular
    .module('tienda')
    .factory('dataMainService', ['mainService', 'purchaseFactory', 'CONFIG',
      dataMainService
    ]);

  function dataMainService(mainService, purchaseFactory, CONFIG) {
    var vm = this;

    // Variables
    vm.categorias = [];
    vm.parametros = [];
    vm.productos = [];
    vm.categoriaSelected = {name: "Categories", id: null};
    vm.productosFiltrados = [];
    vm.filtros = [];
    vm.palabra = null;
    vm.mostrar = false;

    // Services
    vm.mainService = mainService;
    vm.purchaseFactory = purchaseFactory;

    // Methods
    vm.getCategoriasFromStorage = getCategoriasFromStorage();
    vm.count = count;
    vm.hasInCart = hasInCart;
    vm.addToCart = addToCart;
    vm.removeFromCart = removeFromCart;
    vm.addFiltro = addFiltro;
    vm.buscar = buscar;
    vm.setCategoria = setCategoria;
    vm.getProductosFromStorage = getProductosFromStorage;

    getCategoriasFromStorage();
    getProductosFromStorage();
    getParametrosFromStorage();

    return vm;



    function setCategoria(categoria){
      vm.categoriaSelected = categoria;
    }

    /*
      Realiza una busqueda de productos aplicando los diferentes filtros.
     */
    function buscar(palabra){
      vm.productosFiltrados = buscarByCategoria(vm.categoriaSelected, vm.productos);
      if(vm.filtros.length > 0){
        vm.productosFiltrados = aplicarFiltros(vm.filtros, vm.productosFiltrados);
      }
      if((palabra !== undefined) && (palabra !== null) && (palabra.length > 0)){
        vm.productosFiltrados = filtrarByName(palabra, vm.productosFiltrados);
      }



    }




    function aplicarFiltros(filtros, lista){
      var aux = lista;
      for(var i = 0; i < filtros.length; i++){
        switch(filtros[i].tipo){
          case 1:
            aux = filtrarByDisponibilidad(filtros[i], aux);
            break;
          case 3:
            aux = filtrarByStock(filtros[i], aux);
            break;
          case 2:
            aux = filtrarByPrecio(filtros[i], aux);
            break;
          case 4:
            aux.sort(dynamicSort(filtros[i].campo));
            break;
        }
      }
      return aux;
    }



    // Filtra por name
    function filtrarByName(name, lista){
      var tmp = [];
      for(var i = 0; i < lista.length; i++){
        if(lista[i].name.toUpperCase().includes(name.toUpperCase())){
          tmp.push(lista[i]);
        }
      }
      return tmp;
    }

    // Filtra por cantidad en stock
    function filtrarByStock(filtro, lista){
      var tmp = [];
      for(var i = 0; i < lista.length; i++){
        if(lista[i].quantity >= filtro.valor2){
          tmp.push(lista[i]);
        }
      }
      return tmp;
    }

    // Filtra por precio
    function filtrarByPrecio(filtro, lista){
      var tmp = [];
      for(var i = 0; i < lista.length; i++){
        if(filtro.operacion === CONFIG.RANGO){
          if(lista[i].price >= filtro.valor2.min && lista[i].price <= filtro.valor2.max) {
            tmp.push(lista[i]);
          }
        }

        if(filtro.operacion === CONFIG.MAYOR_IGUAL_QUE){
          if(lista[i].price >= filtro.valor2.min){
            tmp.push(lista[i]);
          }
        }

        if(filtro.operacion === CONFIG.MENOR_IGUAL_QUE){
          if(lista[i].price <= filtro.valor2.max){
            tmp.push(lista[i]);
          }
        }
      }
      return tmp;
    }

    // Filtra por Disponibilidad
    function filtrarByDisponibilidad(filtro, lista){
      var tmp = [];
      for(var i = 0; i < lista.length; i++){
        if(lista[i].available === filtro.valor2){
          tmp.push(lista[i]);
        }
      }

      return tmp;
    }


    function buscarByCategoria(categoria, productos){
      var prodFiltrados = [];
      if(categoria.id !== null){
        var lista = convertToArrayCategorias(categoria);
        prodFiltrados = filtrarByCategoria(productos, lista);
      }else{
        prodFiltrados = productos;
      }
      return prodFiltrados;
    }

    // Devuelve una lista de productos que pertenezcan a una categoria y subcategorias de esta.
    function filtrarByCategoria(lista, categorias){
      var listAux = [];
      for(var i = 0; i < lista.length; i++){
        var id = lista[i].sublevel_id;
        if(categorias.indexOf(id) >= 0){
          listAux.push(lista[i]);
        }
      }
      return listAux;
    }


    // Devuelve un array con los id de la categoria y subcategorias
    function convertToArrayCategorias(categoria){
      var lista = [];
      if(categoria.sublevels !== undefined){
        lista.push(categoria.id);
        return lista.concat(recorrerRama(categoria.sublevels, 0));
      }else{
        lista.push(categoria.id);
        return lista;
      }
    }



    // Recorre una rama
    function recorrerRama(lista, index){
      var listaAux = [];
      for(var i = 0; i < lista.length; i++){
        listaAux = listaAux.concat(convertToArrayCategorias(lista[i]));
      }
      return listaAux;
    }

    //Consume el servicio para consultar los parametros de busqueda
    function getParametrosFromStorage(){
      return vm.mainService.getParametros().then(function(response){
          return vm.parametros = response.data;
      });
    }


    // Consume el servicio para consultar las categorias de productos
    function getCategoriasFromStorage(){
      return vm.mainService.getCategorias().then(function(response){
        return vm.categorias =  response.data.categories;
      });
    }

    // Consume el servicio para consultar los productos existentes
    function getProductosFromStorage(){
      return vm.mainService.getProductos().then(function(response){
        vm.productos = response.data.products;
        vm.productosFiltrados = vm.productos;

        return vm.productosFiltrados;
      });
    }

    // Devuelve TRUE si un producto se encuentra en el carrito. FALSE en caso contrario.
    function hasInCart(idProducto){
      var lista =  vm.purchaseFactory.getObject('purchase', []);
      var index = indexOfProducto(lista, idProducto);
      return (index !== -1);
    }


    // Elimina un producto del carrito
    function removeFromCart(idProducto){
      var lista = vm.purchaseFactory.getObject('purchase', []);
      var pos = indexOfProducto(lista, idProducto);
      if(pos !== -1){
        lista.splice(pos, 1);
      }
      vm.purchaseFactory.set('purchase', lista);
    }


    // Agrega un producto al carrito
    function addToCart(producto){
      var lista = vm.purchaseFactory.getObject('purchase', []);
      producto.cantidad = 1;
      lista.push(producto);
      vm.purchaseFactory.set('purchase', lista);
    }

    // Devuelve la cantidad de productos existentes en el carrito
    function count(){
      var lista =  vm.purchaseFactory.getObject('purchase', []);
      return lista.length;
    }

    // Devuelve la posicion de un producto en la lista segun su id. Devuelve -1 cuando el producto no esta en la lista
    function indexOfProducto(lista, id){
      var index = -1;
      for(var i = 0; i < lista.length; i++){
        if(lista[i].id === id){
          index = i;
          break;
        }
      }
      return index;
    }


    // Agrega un objeto filtro a la lista de filtros.
    function addFiltro(filtro){
      var i = indexOfLista(vm.filtros, 'tipo', filtro.tipo);
      if(i >= 0){
        vm.filtros[i] = filtro;
      }else{
        vm.filtros.push(filtro);
      }
    }


    /* Devuelve la posicion de un elemento en una lista. Devuelve -1 en caso de no existir.
      Recibe el array, el key por el que se realizara la busqueda y el objeto a buscar.
    */
    function indexOfLista(array, key, value) {
      var index = -1;
      for (var i = 0; i < array.length; i++) {
        if (array[i][key] === value) {
          index = i;
        }
      }
      return index;
    }

    // Compara un elemento con el siguiente en un array.
    function dynamicSort(property) {
      var sortOrder = 1;
      if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
      }
      return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
      }
    }


  }
})(window.angular)
