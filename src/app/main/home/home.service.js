(function (angular) {
  'use strict';

  angular
    .module('tienda')
    .service('mainService', ['$http',
      mainService
    ]);

  function mainService($http){
    var vm = this;

    // Methods
    vm.getCategorias = getCategorias;
    vm.getProductos = getProductos;
    vm.getParametros = getParametros;

    return vm;




    // Retorna un json con los parametros de busqueda a mostrar
    function getParametros(){
      return $http.get('app/data/parametros.json');
    }


    // Retorna un json con las categorias
    function getCategorias(){
      return $http.get('app/data/categories.json');
    }


    // Retorna un json con los productos
    function getProductos(){
      return $http.get('app/data/products.json');
    }

  }


})(window.angular);
