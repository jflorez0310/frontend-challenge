## Descargar dependencias

En la terminal de comando ingresar lo siguiente:

1. > bower install
2. > npm install



## Test App

Ingresar el siguiente comando.

1. > gulp serve



## Build App

Ingresar el siguiente comando.

1. > gulp build

